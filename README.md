Just a simple example.

Steps to run this example:

1. Clone the project
2. Create virtual environment by running `bin/create_env`
3. Use `source venv/bin/activate` to enter the virtual environment
4. Run Python tests with `bin/run_tests`
5. Run HDL tests with `bin/run_hdl_tests`
