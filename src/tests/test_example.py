'''
Created on May 24, 2019

@author: baumannt
'''
import unittest
import imageio
import numpy as np
from drever.data_handlers.image import ImageData
from example_filter.example_filter import ExampleFilter


class Test(unittest.TestCase):


    def test_example_runner(self):

		# Lets read the test card image as Numpy array and simply use the RGB channels.
        test_card_file = "test_card.png"
        test_card_data = imageio.imread(test_card_file)[::, ::, 0:3]  # the 0:3 indicates the RGB channels
        
		# Create Input Data Container and initialize them with test card image data.
        input_data = ImageData()
        input_data.init_with_data(test_card_data.astype(np.uint16))

		# Set algorithm parameters
        uut_params = {"BLC_OFFSET": 50}

		# Create instance ...
        uut = ExampleFilter(uut_params, input_data, None)
        # .. and run the algorithm.
        uut.run()

		# Get output_data container and write it to file.
        output_data = uut.get_output_data()
        imageio.imwrite("test_card_processed.png", output_data.image_data.astype(np.uint8))


if __name__ == "__main__":  # pragma: no cover
    unittest.main()
