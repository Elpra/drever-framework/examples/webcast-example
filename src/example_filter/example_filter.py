import numpy as np
from drever import video_filter
from drever.data_handlers.image import ImageData


class ExampleFilter(video_filter.VideoFilter):
	
	def run(self):
		
		# The BLC_OFFSET is an algorithm parameter
		blc_offset = self.get_params()["BLC_OFFSET"]
		
		# This is the input data container which provides the input data (Python uses references internally, so this is no data copy).
		in_data_container = self.get_input_data()
		
		# Extract Numpy array from input data container. We convert them to signed integers to ensure proper data handling
		# when dealing with the BLC subtraction.
		in_data = in_data_container.get_image_data().astype(np.int)
		
		# Use Numpy minus operator to subtract blacklevel
		processed_data = in_data - blc_offset
		
		print("Max Value: " + str(np.amax(processed_data)))
		
		# To avoid integer underflow, data has to be clipped like have to be done later in VHDL. Numpyhelps us with an integrated function. 
		processed_data = np.clip(processed_data, 0, 255)

		# We have to create the output data container temporarily ...
		out_data_container = ImageData()
		# and initialize it with the processed data
		out_data_container.init_with_data(processed_data.astype(np.uint16), True, 8)
		
		# And finally we can store the output data container
		self._set_output_data(out_data_container)
		
