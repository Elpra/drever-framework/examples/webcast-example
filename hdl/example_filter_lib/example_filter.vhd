library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity example_filter is
	generic(
		BITDEPTH      : natural range 0 to 16 := 8
	);
	port(
		clk_in        : in  std_logic;
		valid_in      : in  std_logic;
		video_in      : in  std_logic_vector(3*BITDEPTH-1 downto 0);
		valid_q       : out std_logic;
		video_q       : out std_logic_vector(3*BITDEPTH-1 downto 0);
		-- Parameters
		blc_offset_in : in std_logic_vector(BITDEPTH-1 downto 0)
	);
end entity example_filter;

architecture RTL of example_filter is

	signal valid : std_logic                       := '0';
	signal video : std_logic_vector(video_q'range) := (others => '0');

begin

	valid_q <= valid;
	video_q <= video;

	gen_blc_offset : for i in 0 to 2 generate
		signal valid_input  : std_logic;
		signal video_input  : signed(BITDEPTH downto 0);
		signal video_output : unsigned(BITDEPTH-1 downto 0) := (others => '0');
	begin
	
		valid_input <= valid_in;
		video_input <= signed('0' & video_in(BITDEPTH*(i+1)-1 downto BITDEPTH*i));
		
		offset_proc : process (clk_in)
			variable video_processed : signed(BITDEPTH downto 0);
		begin
		
			if (rising_edge(clk_in)) then
			
				video_processed := video_input - signed('0' & blc_offset_in);
				
				if (video_processed < 0 or valid_input = '0') then
					video_output <= (others => '0');
				else
					video_output <= unsigned(video_processed(video_output'range));
				end if;
			
			end if;
		
		end process;
		
		video(BITDEPTH*(i+1)-1 downto BITDEPTH*i) <= std_logic_vector(video_output);
	
	end generate;

	valid_proc : process (clk_in)
	begin
	
		if (rising_edge(clk_in)) then
			valid <= valid_in;
		end if;
	
	end process;
	
end architecture RTL;
