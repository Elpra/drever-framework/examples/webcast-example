library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

-- Include VUnit in Testbench to interact with Python run script
library vunit_lib;
context vunit_lib.vunit_context;

-- Include Drever in Testbench to deal with testvectors
library drever;
use drever.image_data_pkg.all;

-- Include the Unit-Under-Test
library example_filter_lib;

-- Testbench entites should start with tb_
entity tb_example_filter is
	generic (
		runner_cfg   : string := runner_cfg_default;  -- This is needed by VUnit
		output_path  : string;                        -- The output path is the working dir
		IN_FILENAME  : string;                        -- This is the PPM input vector file
		OUT_FILENAME : string;                        -- This is the PPM output vector file
		UUT_PARAMS   : string                         -- IP parameters are coded as string and can be decoded with VUnit's get function
	);
end entity tb_example_filter;

architecture RTL of tb_example_filter is

	-- We use 8 bit per color channel in this example
	constant BITDEPTH : natural := 8;

	signal clk          : std_logic;
	signal blc_offset   : std_logic_vector(BITDEPTH-1 downto 0);
	
	-- Parallel video signals of the source
	signal source_valid : std_logic                               := '0';
	signal source_video : std_logic_vector(3*BITDEPTH-1 downto 0) := (others => '0');
	-- Parallel video signals of the sink
	signal uut_valid    : std_logic                               := '0';
	signal uut_video    : std_logic_vector(3*BITDEPTH-1 downto 0) := (others => '0');

	-- Shared variables are used to handle the PPM image testvectors
	shared variable source_image : t_image;
	shared variable sink_image   : t_image;
	
	-- Same with the image parameters
	signal source_params : t_params;
	signal sink_params   : t_params;

	signal verification_done : boolean := false;

	-- Small helper function because R/G/B has index 0/1/2 in Python and
	-- 23..16/15..8/7..0 in VHDL code
	function conv_ch_index(i : natural) return natural is
	begin
		assert i >= 0 and i < 3
			report "Index out of range!"
			severity failure;
		return 2-i;
	end function;

begin

	gen_clk_proc : process
	begin
		clk <= '0';
		wait for 5 ns;
		clk <= '1';
		wait for 5 ns;
	end process;

	test_runner : process
	begin

		test_runner_setup(runner, runner_cfg);

		-- Loading the input vector file and get its parameters
		source_image.load(output_path & "/" & IN_FILENAME, BITDEPTH);
		source_params <= source_image.get_params;		
		-- Same with output vector file
		sink_image.load(output_path & "/" & OUT_FILENAME, BITDEPTH);
		sink_image.set_params(source_image.get_params);  -- Ensure that both files use same number of channels (currently there's a bug when using a completely black image)
		sink_params <= sink_image.get_params;
		
		wait until verification_done;
		test_runner_cleanup(runner);

	end process;

	-- Let's generate a parallel video source
	video_source_proc : process
		variable ch : natural;
	begin

		blc_offset   <= std_logic_vector(to_unsigned(natural'value(get(UUT_PARAMS, "BLC_OFFSET")), blc_offset'length));
		source_valid <= '0';
		wait for 100 ns;

		for y in 0 to source_params.height-1 loop

			for x in 0 to source_params.width-1 loop

				wait until rising_edge(clk);
				source_valid <= '1';

				for i in 0 to source_params.channels-1 loop
					ch := conv_ch_index(i);
					-- Set parallel video with testvector image data
					source_video((i+1)*BITDEPTH-1 downto i*BITDEPTH) <=
						std_logic_vector(to_unsigned(source_image.get_entry(x, y, ch), BITDEPTH));
				end loop;

			end loop;

			wait until rising_edge(clk);
			source_valid <= '0';
			wait for 100 ns;

		end loop;

		verification_done <= true;

		wait;

	end process;

	uut : entity example_filter_lib.example_filter
		generic map (
			BITDEPTH      => BITDEPTH
		)
		port map (
			clk_in        => clk,
			valid_in      => source_valid,
			video_in      => source_video,
			valid_q       => uut_valid,
			video_q       => uut_video,
			blc_offset_in => blc_offset
		);

	video_sink_proc : process (clk)
		variable x, y      : integer := 0;
		variable was_valid : boolean := false;
		variable ch        : natural;
	begin

		if rising_edge(clk) then

			if uut_valid = '1' then

				was_valid := true;

				for i in 0 to sink_params.channels-1 loop
					ch := conv_ch_index(i);
					assert unsigned(uut_video((i+1)*BITDEPTH-1 downto i*BITDEPTH)) = sink_image.get_entry(x, y, ch)
						report "Video Data Missmatch" & LF &
							"Entry X/Y/Ch: " & integer'image(x) & "/" & integer'image(y) & "/" & integer'image(ch) & LF &
							"Is value:     " & integer'image(to_integer(unsigned(uut_video((i+1)*BITDEPTH-1 downto i*BITDEPTH)))) & LF &
							"Exp. value:   " & integer'image(sink_image.get_entry(x, y, ch))
						severity error;
				end loop;

				x := x + 1;

			else

				-- Increase line counter when line ends
				if was_valid then
					y := y + 1;
				end if;

				was_valid := false;
				x         := 0;
				
				assert unsigned(uut_video) = 0
					report "Video is not Null"
					severity error;

			end if;

		end if;

	end process;

end architecture RTL;
