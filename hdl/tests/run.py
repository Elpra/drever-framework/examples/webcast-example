from vunit import VUnit
import os
import imageio
import numpy as np

import drever
from drever.data_handlers.image import ImageData
import example_filter
from example_filter.example_filter import ExampleFilter


# Create VUnit instance by parsing command line arguments
vu = VUnit.from_argv()

# Create libraries
## Drever HDL sources needs to be known by VUnit
drever.add_drever_hdl_sources(vu)

# Define the directories where VHDL Example Filter and its testbench files are placed.
vhdl_rtl_dir = os.path.join(example_filter.__path__[0], "../../hdl/example_filter_lib/*.vhd")
vhdl_tests_dir = os.path.join(example_filter.__path__[0], "../../hdl/tests/*.vhd")

# The Example Filter is put in its own VHDL library
example_filter_lib = vu.add_library("example_filter_lib")
example_filter_lib.add_source_files(vhdl_rtl_dir)

# The testbench is also put in its own library
test_lib = vu.add_library("test_lib")
test_lib.add_source_files(vhdl_tests_dir)
tb_example_filter = test_lib.entity("tb_example_filter")

# Let's use a 200x100 random image with 3 color channels
random_data = ImageData({
    "width":    200,
    "height":   100,
    "bitdepth":  8,
    "channels":  3
})
random_data.init_with_random(0)

test_card_file = "test_card.png"
test_card_pixels = imageio.imread(test_card_file)[::,::,:3]
test_card_data = ImageData()
test_card_data.init_with_data(test_card_pixels.astype(np.uint16), True, 8)

# Let's define different UUTs with different test cases
uuts = [
    ExampleFilter(
        params={"BLC_OFFSET": 0},
        input_data=random_data,
        dump_filename="random000.ppm"
    ),
    ExampleFilter(
        params={"BLC_OFFSET": 10},
        input_data=random_data,
        dump_filename="random010.ppm"
    ),
    ExampleFilter(
        params={"BLC_OFFSET": 255},
        input_data=random_data,
        dump_filename="random255.ppm"
    ),
    ExampleFilter(
        params={"BLC_OFFSET": 50},
        input_data=test_card_data,
        dump_filename="test_card.ppm"
    )
]

for uut in uuts:

    config_name = "dumpfile=%s" % (uut.dump_filename)

    params = uut.params

    generics = uut.vunit_generate_generics()
    generics["UUT_PARAMS"] = \
        ", ".join(["%s:%s" % (key, str(params[key])) for key in params])

    tb_example_filter.add_config(
        name="rgb_swap:" + config_name,
        pre_config=uut.vunit_pre_config,
        generics=generics
    )


vu.main()
